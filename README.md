# sec

This is a hobby project that i use to better read the stock purchase info from sec.gov form 13-k. It is a symfony console
project which read an xml file from sec.gov and show a table with the stock data like price volume etc.


## Usage
bin/console app:sec secfilename.xml

## Plans
I am in the process of adding an extra column with current prices of the stock but need to find a good free ticker api.