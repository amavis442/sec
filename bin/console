#!/usr/bin/env php
<?php
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Debug\Debug;

use App\Service\AlphavantageWriterService;
use App\Reader\XmlReader;
use App\Reader\AlphavantageJSONReader;
use App\Reader\ObjectFileReader;
use App\Writer\FileWriter;
use App\Command\SecReaderCommand;
use App\Command\SecDataCollectorCommand;
use App\Service\SecService;

set_time_limit(0);

require __DIR__.'/../vendor/autoload.php';

$input = new ArgvInput();
if (null !== $env = $input->getParameterOption(['--env', '-e'], null, true)) {
    putenv('APP_ENV='.$_SERVER['APP_ENV'] = $_ENV['APP_ENV'] = $env);
}

if ($input->hasParameterOption('--no-debug', true)) {
    putenv('APP_DEBUG='.$_SERVER['APP_DEBUG'] = $_ENV['APP_DEBUG'] = '0');
}

require dirname(__DIR__).'/config/bootstrap.php';

if (isset($_SERVER['APP_DEBUG']) && $_SERVER['APP_DEBUG']) {
    umask(0000);

    if (class_exists(Debug::class)) {
        Debug::enable();
    }
}

$config = [];
if (file_exists(dirname(__DIR__).'/config/config.php')) {
    $config = require dirname(__DIR__).'/config/config.php';
}

$dataFile = dirname(__DIR__).'/var/log/data.txt';
// The writer to prefetch data and store it in a file
$jsonReader = new AlphavantageJSONReader();
$fileWriter = new FileWriter();
$fileWriter->setFilename($dataFile);
$writerService = new AlphavantageWriterService($config);
$writerService->setReader($jsonReader);
$writerService->setWriter($fileWriter);

$collectorCommand = new SecDataCollectorCommand();
$collectorCommand->setWriterService($writerService);

$objectReader = new ObjectFileReader();
$objectReader->setSource($dataFile);
$secService = new SecService($config, new XmlReader(), $objectReader);
$secCommand = new SecReaderCommand();
$secCommand->setService($secService);


$application = new Application();
$application->add($secCommand);
$application->add($collectorCommand);
$application->run();
