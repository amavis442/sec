<?php
namespace App\Model;

class Symbol 
{
    public $name;
    public $quote;
    public $open;
    public $high;
    public $low;
    public $price;
    public $volume;
    public $date;
    public $previous_close;
    public $change;
    public $change_percentage;

    public function getName(): string
    {
        return $this->name;
    }
    
    public function getQuote(): string
    {
        return $this->quote;
    }

    public function getOpen(): float
    {
        return $this->open;
    }

    public function getHigh(): float
    {
        return $this->high;
    }
    
    public function getLow(): float
    {
        return $this->Low;
    }
    
    public function getPrice(): float
    {
        return $this->price;
    }

    public function getVolume(): int
    {
        return $this->volume;
    }
    
    public function getDate(): string
    {
        return $this->open;
    }

    public function getPreviousClose(): float
    {
        return $this->previous_close;
    }

    public function getChange(): float
    {
        return $this->change;
    }

    public function getChangePercentage(): string
    {
        return $this->change_percentage;
    }
}