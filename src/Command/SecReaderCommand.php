<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Helper\Table;
use App\Reader\ReaderInterface;

class SecReaderCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:sec';
    private $readerService;

    protected function configure(): void
    {
        $this
        ->setDescription('Reads a sec xml file where you can get data like average price for stocks.')
        ->setHelp('This command allows you to create a user...')
        ->addArgument('filename', InputArgument::REQUIRED, 'The xml file to read.')
        ->addArgument('quotesfile', InputArgument::OPTIONAL, 'The quotes file to read.', 'data.txt');
    }

    public function setService($readerService): void
    {
        $this->readerService = $readerService;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $filename = $input->getArgument('filename');
        $this->readerService->setXmlFilename($filename);

        //$quotesFile = $input->getArgument('quotesfile');
        //$this->readerService->setQuotesFilename($quotesFile);
        
        $rows = $this->readerService->fetch();

        $table = new Table($output);
        $table->setHeaders(['slug', 'issuer', 'shares', 'value', 'quote', 'price', 'current marketprice']);
        $table->addRows($rows);
        $table->render();
    }
}
