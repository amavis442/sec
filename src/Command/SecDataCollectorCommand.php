<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Helper\Table;

use App\Service\WriterServiceInterface;

class SecDataCollectorCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:collector';
    private $writerService;

    protected function configure(): void
    {
        $this
        ->setDescription('Fetches data from an external source and formats the data and writes it to a file for later processing (REAL SLOW).')
        ->setHelp('This command creates a file (default data.txt) with symbol data ...')
        ->addArgument('filename', InputArgument::OPTIONAL, 'File to write to and use later for processing.')
        ;
    }

    public function setWriterService(WriterServiceInterface $writerService): void
    {
        $this->writerService = $writerService;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Fetching data');
        $output->writeln('This can take a while if the api only permits 5 calls per minute');
        $this->writerService->store();
        $output->writeln('..... Done.');
    }
}
