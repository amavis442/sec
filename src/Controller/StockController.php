<?php

namespace App\Controller;

use App\Service\ReaderServiceInterface;
use App\Service\FileWriterServiceInterface;

class ReadController
{
    private $secReader;
    private $tickerWriter;
    
    public function __construct(ReaderServiceInterface $secReader, FileWriterServiceInterface $tickerWriter)
{
        $this->secReader = $secReader;
        $this->tickerWriter = $tickerWriter;

}

    public function index()
    {

    }

}
