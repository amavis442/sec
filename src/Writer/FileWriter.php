<?php
namespace App\Writer;

use App\Writer\FileWriterInterface;

class FileWriter implements FileWriterInterface
{
    private $filename;

    public function setFilename(string $filename): void
    {
        $this->setTarget($filename);
    }

    public function setTarget(string $target = ''): void
    {
        $this->filename = $target;
    }

    public function store($data): void
    {
        if (!isset($this->filename)) {
            throw new \Exception('Please make sure we have a file to write to.');
        }
        file_put_contents($this->filename, $data);
    }
}