<?php
namespace App\Writer;
use App\Writer\WriterInterface;

interface FileWriterInterface extends WriterInterface
{
    /** 
     * File to store data in
     */
    public function setFilename(string $filename): void;
}