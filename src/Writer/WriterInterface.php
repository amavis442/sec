<?php
namespace App\Writer;

interface WriterInterface
{

     /**
     * Where should to the data be written to (can be database, other server, file, etc)?
     */
    public function setTarget(string $target = ''): void;


    /**
     * Reads data from an external source like an url and stores the formatted data
     * in an serialized object in a file for later processing. 
     */
    public function store($data): void;
}