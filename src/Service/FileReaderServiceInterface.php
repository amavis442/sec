<?php
namespace App\Service;

use App\Service\ReaderServiceInterface;

interface FileReaderServiceInterface extends ReaderServiceInterface
{
    /**
     * What writer do we like to use: this can be a file, RestFull, soap, database whatever.
     */
    public function setFilename(string $filename): void;
}