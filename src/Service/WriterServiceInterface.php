<?php
namespace App\Service;

use App\Writer\WriterInterface;

interface WriterServiceInterface
{
    /**
     * What writer do we like to use: this can be a file, RestFull, soap, database whatever.
     */
    public function setWriter(WriterInterface $writer): void;

      /**
     * Reads data from an external source like an url and stores the formatted data
     * in an serialized object in a file for later processing. 
     */
    public function store(): void;
}