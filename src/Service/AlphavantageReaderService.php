<?php
namespace App\Service;

use App\Reader\ReaderInterface;
use App\Service\ReaderServiceInterface;
use App\Model\Symbol;
/**
 * Transform the data from the api call to a generic format so if we use
 * another service to get the symbol data. We can write a new service and that should be it.
 */
class AlphavantageReaderService implements ReaderServiceInterface
{
    protected $reader;

    public function setReader(ReaderInterface $reader): void
    {
        $this->reader = $reader;
    }

    public function fetch(string $symbol = ''): ?Symbol
    {
        $data = $this->reader->read($symbol);

        if (!isset($data['Global Quote']) || count($data['Global Quote']) < 1) {
            return null;
        }

        $data = $data['Global Quote'];
        $symbol = new Symbol();
        $symbol->name = $data['01. symbol'];
        $symbol->open = (float)$data["02. open"];
        $symbol->high = (float)$data["03. high"];
        $symbol->low = (float)$data["04. low"];
        $symbol->price = (float)$data["05. price"];
        $symbol->volume = (int)$data["06. volume"];
        $symbol->date = (string)$data["07. latest trading day"];
        $symbol->previous_close = (float)$data["08. previous close"];
        $symbol->change = (float)$data["09. change"];
        $symbol->change_percentage = (string)$data["10. change percent"];

        return $symbol;
    }
}
