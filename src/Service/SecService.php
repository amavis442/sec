<?php
namespace App\Service;

use App\Reader\ReaderInterface;

/**
 * This service reads from 2 sources an xml file and a previous created file with a collection of
 * symbol object retaint from an api call to an external ticker service.
 * It reads the xml file from SEC and formats the data in rows and add the current marketprice (not realtime)
 * 
 */
class SecService
{
    private $xmlReader;
    private $objectFileReader;
    private $xmlFilename;
    private $tickerCollection;

    public function __construct($config = [], ReaderInterface $xmlReader, ReaderInterface $objectFileReader)
    {
        $this->xmlReader = $xmlReader;
        $this->objectFileReader = $objectFileReader;
    }

    /**
     * Where can the xml file be found
     */
    public function setXmlFilename(string $xmlFilename): void
    {
        if (!file_exists($xmlFilename)) {
            throw new \Exception('Xml file does not exist');
        }
        $this->xmlReader->setSource($xmlFilename);
        $this->xmlFilename = $xmlFilename;
    }

    /**
     * Return an array to be used as desired for instance a table
     */
    public function fetch(): array
    {
        if (!isset($this->xmlFilename)) {
            throw new \Exception('Please add an xml file to read');
        }
        
        $data = $this->xmlReader->read();
        $this->tickerCollection = $this->objectFileReader->read();

        $rows = [];
        foreach ($data as $slug => $item) {
            $issuer = $item['issuer'];
            $price = $this->getCurrentPrice($issuer);
            $quote = $this->getQuote($issuer);
            $rows[] = [$slug, $issuer, $item['shares'], $item['value'] * 1000, $quote, "$".round($item['average'], 2), "$".$price];
        }

        return $rows;
    }

    /**
     * If the objectfile has a symbol object for the requested symbolname,
     * it will return the marketprice of a not found if symbol is not present.
     */
    private function getQuote(string $issuer): string
    {
        $quote = '';
        if (isset($this->tickerCollection[$issuer]) && !is_null($this->tickerCollection[$issuer])) {
            $dataSymbol = $this->tickerCollection[$issuer];
            if ($dataSymbol) {
                $quote = $dataSymbol->getQuote();
            } 
        }
        return $quote;
    }


    /**
     * If the objectfile has a symbol object for the requested symbolname,
     * it will return the marketprice of a not found if symbol is not present.
     */
    private function getCurrentPrice(string $issuer): string
    {
        $price = 0.0;
        if (isset($this->tickerCollection[$issuer]) && !is_null($this->tickerCollection[$issuer])) {
            $dataSymbol = $this->tickerCollection[$issuer];
            if ($dataSymbol) {
                $price = $dataSymbol->getPrice();
            } else {
                $price = " SYMBOL NOT FOUND";
            }
        }
        return $price;
    }
}
