<?php
namespace App\Service;

use App\Model\Symbol;
use App\Reader\ReaderInterface;

interface ReaderServiceInterface
{
    /**
     * Which reader should we use, this can be an external url, a file, a database etc.
     */
    public function setReader(ReaderInterface $reader): void;

    /**
     * Fetch a bulk of data or just a single item and return a Symbol object
     */
    public function fetch(string $symbol = ''): ?Symbol;
}