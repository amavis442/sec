<?php
namespace App\Service;

use App\Service\WriterServiceInterface;

interface FileWriterServiceInterface extends WriterServiceInterface
{
    /**
     * What writer do we like to use: this can be a file, RestFull, soap, database whatever.
     */
    public function setFilename(string $filename): void;
}