<?php
namespace App\Service;

use App\Service\ReaderServiceInterface;
use App\Service\WriterServiceInterface;
use App\Reader\ReaderInterface;
use App\Writer\WriterInterface;

/**
 * Transform the data from the api call to a generic format so if we use
 * another service to get the symbol data. We can write a new service and that should be it.
 */
class AlphavantageWriterService extends AlphavantageReaderService implements ReaderServiceInterface, WriterServiceInterface
{
    private $translate = [];
    private $writer;

    public function __construct($config = [], ReaderInterface $reader = null, WriterInterface $writer = null)
    {
        if (!is_null($reader)){
            $this->reader = $reader;
        }

        if (!is_null($writer)){
            $this->writer = $writer;
        }

        if (isset($config['pairs'])) {
            $this->translate = $config['pairs'];
        }
    }

    public function setWriter(\App\Writer\WriterInterface $writer): void
    {
        $this->writer = $writer;
    }

    /**
     * Get the ticker data, the api of alphavantage only supports 1 ticker per call,
     * and save an object to a file so we can use it later with the file from SEC to show
     * extra data like current price.
     */
    public function store(): void
    {
        $keys = array_keys($this->translate);
        $lastElement = end($keys);
        $data = [];
        if (count($this->translate) > 0) {
            foreach ($this->translate as $name => $quote) {
                $symbol = $this->fetch($quote);
                $symbol->quote = $quote;
                $data[$name] = $symbol;
                if ($name != $lastElement) {
                    sleep(22); // more to come
                }
            }
            $saveString = serialize($data);
            $this->writer->store($saveString);
        }
    }
}
