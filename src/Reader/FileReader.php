<?php

namespace App\Reader;

abstract class FileReader
{
    protected $filename;

    public function setInputFile(string $source = ''): void
    {
        $this->filename = $source;
    }

    public function setSource(string $source = ''): void
    {
        $this->setInputFile($source);
    }
}