<?php

namespace App\Reader;

use App\Reader\ReaderInterface;
use GuzzleHttp\Client;

/**
 * Returns data for a symbol from a sites api in json format
 *
 * Only allows 5 calls per minute and 500 per day
 *
 * @see https://www.alphavantage.co/documentation/
 */
class AlphavantageJSONReader implements ReaderInterface
{
    const URL = "https://www.alphavantage.co/query?function=GLOBAL_QUOTE&datatype=json&outputsize=full&symbol=[SYMBOL]&apikey=[APIKEY]";
    private $url;

    public function __construct()
    {
        $this->url = self::URL;
    }

    public function setSource(string $source = ''): void
    {
        if (!isset($source) || empty($source)) {
            $this->url = self::URL;
        } else {
            $this->url = $source;
        }
    }

    public function read(string $symbol = '') : ?array
    {
        $url = str_replace('[SYMBOL]', $symbol, $this->url);
        $url = str_replace('[APIKEY]', getenv('APIKEY'), $url);
        $client = new Client();

        $res = $client->request('GET', $url);
        if ($res->getStatusCode() == 200) {
            $data = json_decode($res->getBody(), true);

            return $data;
        }
        return null;
    }
}
