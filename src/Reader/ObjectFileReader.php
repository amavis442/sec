<?php

namespace App\Reader;

use App\Reader\ReaderInterface;
use App\Reader\FileReader;

/**
 * Returns an array of symbol objects (model) to be used as desired.
 */
class ObjectFileReader extends FileReader implements ReaderInterface
{
    public function read(string $symbol = '') : ?array
    {
        if (!file_exists($this->filename)) {
            throw new \Exception('File does not exist.');
        }

        $fileData = file_get_contents($this->filename);
        $data = unserialize($fileData);
        return $data;
    }
}
