<?php
namespace App\Reader;

interface ReaderInterface
{
    /**
     * Where can to source be found?
     */
    public function setSource(string $source = ''): void;

    /**
     * Read the source and process it like you want.
     */
    public function read(string $symbol = '') : ?array;
}
