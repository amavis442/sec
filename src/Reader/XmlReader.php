<?php

namespace App\Reader;

use App\Reader\ReaderInterface;
use App\Reader\FileReader;

class XmlReader extends FileReader implements ReaderInterface
{
    public function read(string $symbol = ''): ?array
    {
        if (is_null($this->filename)) {
            throw new \Exception('No filename given');
        }

        $data = [];

        $domDocument = new \DOMDocument();
        $domDocument->load($this->filename);

        $nodes = $domDocument->getElementsByTagName('infoTable');

        foreach ($nodes as $node) {
            $issuer = trim($node->getElementsByTagName('nameOfIssuer')[0]->nodeValue);
            $slug = str_replace(" ", "-", strtolower($issuer));

            $value = (int)$node->getElementsByTagName('value')[0]->nodeValue;
            $shares = (int)$node->getElementsByTagName('sshPrnamt')[0]->nodeValue;
            $marketPrice = ($value* 1000) / $shares;

            if (!isset($data[$slug])) {
                $data[$slug]['issuer'] = "";
                $data[$slug]['data'] = [];
                $data[$slug]['average'] = 0;
                $data[$slug]['shares'] = 0;
                $data[$slug]['value'] = 0;
            }
            $data[$slug]['issuer'] = $issuer;
            $data[$slug]['data'][] = [$shares, $value, $marketPrice];
            $data[$slug]['shares'] += $shares;
            $data[$slug]['value'] += $value;
            $data[$slug]['average'] += $marketPrice;
        }

   
        foreach ($data as &$item) {
            $div = count($item['data']);
            $item['average'] = ($item['average'] / $div);
        }
        
        if (count($data) > 0) {
            return $data;
        }

        return null;
    }
}
